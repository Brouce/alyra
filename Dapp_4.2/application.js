async function createMetaMaskDapp() {
    try {
        // Demande à MetaMask l'autorisation de se connecter
        const addresses = await ethereum.enable();
        const address = addresses[0]
        // Connection au noeud fourni par l'objet web3
        const provider = new ethers.providers.Web3Provider(ethereum);
        dapp = { address, provider };
        console.log(dapp)
    } catch(err) {
        // Gestion des erreurs
        console.error(err);
    }
}

async function getData() {
    balance();
    latestBlockNb();
    currentGas();
    contrat();
}

async function balance() {
    dapp.provider.getBalance(dapp.address).then((balance) => {
        let etherString = ethers.utils.formatEther(balance);
        console.log('Balance: ' + etherString);
        document.getElementById('accountBalance').innerHTML =  etherString;
    });
}

async function latestBlockNb() {
    dapp.provider.getBlockNumber().then((blockNb) => {
        console.log('Lastest block: ' + blockNb);
        document.getElementById('currentBlockHeight').innerHTML = blockNb;
    })
}

async function currentGas() {
    dapp.provider.getGasPrice().then((gasPrice) => {
        // gasPrice is a BigNumber; convert it to a decimal string
        gasPriceString = gasPrice.toString();    
        console.log('Current gas price: ' + gasPriceString);
        document.getElementById('currentGasPrice').innerHTML =  gasPriceString;
    });
}

async function contrat() {
    // contrat cours 0x451875bdd0e524882550ec1ce52bcc4d0ff90eae
    // contrat brouce 0x8c88256a21Fc0063a419612689a655Ab5c14f8AC et 0xf52dd6a27131910758da7e60a6302e473c7b6db3
    // compte (metamask) 0x1810d8D39a7fDaD99B47124048658c947A237CC6
    let contratCredibilite = new ethers.Contract('0xf52dd6a27131910758da7e60a6302e473c7b6db3', abi, dapp.provider.getSigner());
    console.log("abi ok");

    var cred = await contratCredibilite.cred(dapp.address); 
    console.log("Cred: " + cred);

    var hash = await contratCredibilite.produireHash("https://remix.ethereum.org/#optimize=false&version=soljson-v0.5.4+commit.9549d8ff.js"); 
    console.log("Hash devoir: " + hash);

    var trx = await contratCredibilite.remettre(hash); // ouvre Metamask
    await trx.wait(); // attend que la trx soit minée
    // console.log("classement: " + trx.toString());

    var cred = await contratCredibilite.cred(dapp.address); 
    console.log("Cred: " + cred);
}

let abi = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "dev",
				"type": "bytes32"
			}
		],
		"name": "remettre",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "destinataire",
				"type": "address"
			},
			{
				"name": "valeur",
				"type": "uint256"
			}
		],
		"name": "transfer",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "cred",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "url",
				"type": "string"
			}
		],
		"name": "produireHash",
		"outputs": [
			{
				"name": "",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "pure",
		"type": "function"
	}
]