public class VarInt {

    /**
     * longueur en octet du varint, en incluant le préfixe
     */
    public int length;

    /**
     * Valeur du varint
     */
    public int value;

    public VarInt(String varInt) {

        varInt = varInt.toLowerCase();

        if (varInt.length()%2 != 0) {
            varInt = "0" + varInt;
            System.out.println("Attention varint de taille impaire: " + varInt);
        }

        // 2 octets
        if (varInt.substring(0,2).equals("fd")) {
            length = 2;
            value = Integer.valueOf(varInt.substring(2,6), 16);
        }

        // 4 octets
        else if (varInt.substring(0,2).equals("fe")) {
            length = 4;
            value = Integer.valueOf(varInt.substring(2,10), 16);
        }

        // 8 octets
        else if (varInt.substring(0,2).equals("ff")) {
            length = 8;
            value = Integer.valueOf(varInt.substring(2,18), 16);
        }

        // 1 octet
        else {
            length = 1;
            value = Integer.valueOf(varInt.substring(0,2),16); // varint < 253
        }

        //System.out.println("VarInt: " + varInt.substring(0,length*2) + " value: " + value + " octets suivants");
    }
}
