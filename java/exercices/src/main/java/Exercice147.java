import org.apache.commons.codec.digest.DigestUtils;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.util.encoders.Hex;

import java.util.Deque;
import java.util.LinkedList;

public class Exercice147 {

    /* sources:
     http://learnmeabitcoin.com/glossary/public-key
     https://medium.com/coinmonks/bitcoin-p2pkh-transaction-breakdown-bb663034d6df
    */

    // Liste les opérations possibles
    private static final byte OP_0 = (byte) 0x00;
    private static final byte OP_1 = (byte) 0x51;
    private static final byte OP_DUP = (byte) 0x76;
    private static final byte OP_ADD = (byte) 0x93;
    private static final byte OP_EQUALVERIFY = (byte) 0x88;
    private static final byte OP_HASH160 = (byte) 0xa9;
    private static final byte OP_CHECKSIG = (byte) 0xac;
    private static final byte OP_CHECKLOCKTIMEVERIFY = (byte) 0xb1;

    String sequence = "";

    /**
     * Le séparateur est un espace
     *
     * @param scriptSig    (unlocking code) signature de la clé publique (20) + clé publique
     * @param scriptPubKey (locking script) le script en notation polonaise inversée à appliquer,
     *                     au format OP_DUP OP_HASH160 [hash of public key] OP_EQUALVERIFY OP_CHECKSIG
     */
    private boolean verificationP2PKH(String scriptSig, String scriptPubKey) {

        // Supprime la notation hexa "0x" des paramètres d'entrée
        if (scriptSig.startsWith("0x")) scriptSig = scriptSig.substring(2);
        if (scriptPubKey.startsWith("0x")) scriptPubKey = scriptPubKey.substring(2);

        // Mets les hexa en lowercase
        scriptSig = scriptSig.toLowerCase();
        scriptPubKey = scriptPubKey.toLowerCase();

        // vérifie que scriptPubKey est bien une séquence P2PKH
        if (!(scriptPubKey.startsWith("76a9") && (scriptPubKey.endsWith("88ac")))) {
            throw new RuntimeException("Not a P2PKH script !");
        }

        // scriptSig: sort la signature de la clé publique, 72 octets
        VarInt vi = new VarInt(scriptSig.substring(0, 18)); // +18 = max varint
        int index = vi.length * 2 + vi.value * 2; // index donne la dernière position de scriptSig prise en compte
        String pubKeySign = scriptSig.substring(vi.length * 2, index);
        System.out.println("scriptSig->pubKeySign: " + pubKeySign + " length " + pubKeySign.length() / 2 + " octets");

        // scriptSig: sort la clé publique 33 octets, en général compréssée, préfix 02 ou 03
        VarInt vi1 = new VarInt(scriptSig.substring(index, index + 18)); // +18 = max varint
        int index1 = vi1.length * 2 + vi1.value * 2;
        String pubKey = scriptSig.substring(index + vi1.length * 2, index + index1);
        System.out.println("scriptSig->pubKey: " + pubKey + " length " + pubKey.length() / 2 + " octets");
        System.out.println("");

        // sort le "hash of public key"
        // en première position il y a un varint qui donne sa longueur en octets
        // qui est toujours 0x14 = 20 octets car le hash est issu d'un ripemd160
        VarInt vi2 = new VarInt(scriptPubKey.substring(4, 22));
        String hashPublicKey = scriptPubKey.substring(4 + vi2.length * 2, 4 + vi2.length * 2 + vi2.value * 2); // 4+40
        System.out.println("scriptPubKey->hashPublicKey: " + hashPublicKey + " length " + hashPublicKey.length() / 2 + " octets");


        // alimente une nouvelle pile
        Deque<String> stack = new LinkedList<>();
        stack.add(pubKeySign);
        stack.add(pubKey);
        stack.add(stack.getLast()); // OP_DUP copie la clé publique sur la pile
        stack.add(hash160(stack.pollLast())); // OP_HASH160 de la copie de la clé publique
        stack.add(hashPublicKey); // ajoute sur la pile la clé publique de scriptPubKey

        // OP_EQUALVERIFY le hashPubKey fourni et celui que l'on vient de calculer doivent être egaux
        if (!stack.pollLast().equals(stack.pollLast())) {
            return false;
        }

        // OP_CHECKSIG

        return true;
    }

    /**
     * @param pubKey la clé publique
     * @return le hash de la clé: applique SHA-256 puis RIPEMD160
     */
    private String hash160(String pubKey) {

            System.out.println("");
            System.out.println("OP_HASH160: pubKey " + pubKey);

            // calcule un hash sha256, 32 octets
            byte[] sha256 = DigestUtils.sha256(pubKey.getBytes());
            System.out.println("sha256: " + Hex.toHexString(sha256) + " length: " + sha256.length + " octets");


            // calcule un hash ripemd160, 20 octets
            RIPEMD160Digest hash20 = new RIPEMD160Digest();
            hash20.update(sha256, 0, sha256.length);
            byte[] rpd160 = new byte[hash20.getDigestSize()];
            hash20.doFinal(rpd160, 0);
            System.out.println("ripemd160: " + Hex.toHexString(rpd160) + " length: " + rpd160.length + " octets");

            return Hex.toHexString(rpd160);
    }


    public static void main(String[] args) {

        Exercice147 stack = new Exercice147();

        boolean test = stack.verificationP2PKH("0x483045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e40"
                + "5c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001210372cc7efb1961962bba20db0c6a3eebdde0ae60698"
                + "6bf76cb863fa460aee8475c", "0x76a9147c3f2e0e3f3ec87981f9f2059537a355db03f9e888ac");

        System.out.println("");
        System.out.println(test ? "P2PKH vérifié !" : "P2PKH ko");
    }
}