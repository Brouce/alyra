import org.apache.commons.codec.digest.DigestUtils;
import org.bitcoinj.core.Base58;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.util.encoders.Hex;

import java.security.SecureRandom;

/**
 * Sources:
 * http://lenschulwitz.com/base58
 * https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses
 */
public class Exercice135 {


    public static void main(String[] args) {

        try {
            System.out.println("Exercice 1.3.5 : Génèrer une adresse type bitcoin");
            genererAdresseBtc();

        } catch (RuntimeException re) {
            System.err.println(re.getMessage());
        }
    }


    /**
     * Fabrique une adressse Bitcoin
     *
     * @return l'adresse en base 58
     */
    private static String genererAdresseBtc() {
        // Tous les calculs sont effectués avec des tableaux d'octets
        // Les conversions en hexa se font à l'affichage uniquement

        // get a random array of values
        SecureRandom random = new SecureRandom();
        byte[] values = new byte[32];
        random.nextBytes(values); // rempli le tableau avec des valeur aléatoires
        System.out.println("random Array de " + values.length + " bytes");


        // calcule un hash sha256, 32 octets
        byte[] sha256 = DigestUtils.sha256(values);
        System.out.println("sha256: " + Hex.toHexString(sha256) + " length: " + sha256.length + " octets");

        // calcule un hash ripemd160, 20 octets
        RIPEMD160Digest hash20 = new RIPEMD160Digest();
        hash20.update(sha256, 0, sha256.length);
        byte[] rpd160 = new byte[hash20.getDigestSize()];
        hash20.doFinal(rpd160, 0);
        System.out.println("ripemd160: " + Hex.toHexString(rpd160) + " length: " + rpd160.length + " octets");


        // Ajoute le préfixe '00' à l'adresse
        byte[] addrTmp = new byte[rpd160.length + 1]; // 21 octets
        addrTmp[0] = 0x00; // préfix de 1 byte 0x00
        System.arraycopy(rpd160, 0, addrTmp, 1, rpd160.length); // reste de l'adresse
        System.out.println("Adresse temp: " + Hex.toHexString(addrTmp) + " length: " + addrTmp.length + " octets");

        // calcule un double hash de l'adresse précédente
        byte[] doubleHash = DigestUtils.sha256(addrTmp);
        doubleHash = DigestUtils.sha256(doubleHash);
        System.out.println("doubleHash: " + Hex.toHexString(doubleHash));

        // 4 premiers octets de doubleHash à la fin de l'adresse BTC
        byte[] addrBtc = new byte[addrTmp.length + 4]; // 25 octets
        System.arraycopy(addrTmp, 0, addrBtc, 0, 21);  // copie de l'adresse en 0-21
        System.arraycopy(doubleHash, 0, addrBtc, 21, 4); // copie des 4 derniers octets
        System.out.println("Adresse btc: " + Hex.toHexString(addrBtc) + " length: " + addrBtc.length + " octets");

        // Converti en base58
        String addrBtcB58 = Base58.encode(addrBtc);
        System.out.println("Adresse btc en base 58: " + addrBtcB58 + " length: " + addrBtcB58.length() + " char");

        return addrBtcB58;
    }
}
