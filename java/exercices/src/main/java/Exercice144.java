import java.math.BigInteger;

/**
 * Exercice 1.4.4 : écrire un script
 *
 * sources:
 * https://klmoney.wordpress.com/bitcoin-dissecting-transactions-part-2-building-a-transaction-by-hand/
 */
public class Exercice144 {

    public static void main(String[] args) {

        System.out.println("Exercice 1.4.4 : écrire un script");

        String trx = "0100000001f129de033c57582efb464e94ad438fff493cc4de4481729b859712368582" +
                "75c2010000006a4730440220155a2ea4a702cadf37052c87bfe46f0bd24809759acff8" +
                "d8a7206979610e46f6022052b688b784fa1dcb1cffeef89e7486344b814b0c578133a7" +
                "b0bce5be978a9208012103915170b588170cbcf6380ef701d19bd18a526611c0c69c62" +
                "d2c29ff6863d501affffffff02ccaec817000000001976a9142527ce7f0300330012d6" +
                "f97672d9acb5130ec4f888ac18411a000000000017a9140b8372dffcb39943c7bfca84" +
                "f9c40763b8fa9a068700000000";

        decodeTrx(trx);


        /* Décomposition de la transaction

        01000000 version 4 octets
        01 nb of inputs

        f129de033c57582efb464e94ad438fff493cc4de4481729b85971236858275c2 previous trx hash (LE) 32 octets
        01000000 previous trx index LE 4
        6a script length de l'input 01,  106 octets

        unlocking script -> signature
        47 signature length 71 octets
        30440220155a2ea4a702cadf37052c87bfe46f0bd24809759acff8d8a7206979610e46f6022052b688b784fa1dcb1cffeef89e7486344b814b0c578133a7b0bce5be978a920801 71 octets

        unlocking script -> clé publique
        21 pubKey length 33 octets
        03915170b588170cbcf6380ef701d19bd18a526611c0c69c62d2c29ff6863d501a 33 octets

        ffffffff sequence

        02 nb of ouputs

        output 01:
        ccaec81700000000 prix en satochis
        19 locking script length 25 octets
        76a9142527ce7f0300330012d6f97672d9acb5130ec4f888ac  locking script 25 octets P2PKH "76a9...88ac"

        ouput 02:
        18411a0000000000 prix en satochis
        17 locking script length 23 octets
        a9140b8372dffcb39943c7bfca84f9c40763b8fa9a0687 locking script P2SH ?

        00000000 locktime

         */
    }

    /* Décomposition de la transaction:
     */
    private static void decodeTrx(String trx) {
        trx = trx.toLowerCase();

        // corrige la taille de la trx: lenght trx ko si sa taille est impaire
        if (trx.length() % 2 != 0) trx = "0" + trx;

        // position d'un curseur pour parcourir la transaction
        int pos = 0;

        String version = trx.substring(0, pos=8);
        print("Version 4 octets", version);

        String nbInputs = trx.substring(pos, pos+=2); // 1 octet
        print("Nb of inputs", nbInputs);

        for (int i=0 ; i<Integer.valueOf(nbInputs) ; i++) {

            String hash = trx.substring(pos, pos+=64); // 32 octets
            print("Previous trx hash (LE)", hash);

            String index = trx.substring(pos, pos+=8); // 4 octets
            print("Previous trx index (LE)", index);

            VarInt scriptLength = new VarInt(trx.substring(pos, pos+18)); // 9 bytes max
            print("Unlocking Script length", scriptLength.value);
            pos = pos + scriptLength.length*2;

            // Unlocking script
            VarInt signLength = new VarInt(trx.substring(pos, pos+18));
            print("Unlocking script -> signature length", signLength.value);

            String signature = trx.substring(pos+=signLength.length*2, pos+=signLength.value*2);
            print("Unlocking script -> signature", signature);

            VarInt pubKeyLength = new VarInt(trx.substring(pos, pos+18));
            print("Unlocking script -> clé publique length", pubKeyLength.value);

            String pubKey = trx.substring(pos+=pubKeyLength.length*2, pos+=pubKeyLength.value*2);
            print("Unlocking script -> clé publique", pubKey);

            String sequence = trx.substring(pos, pos+=8); // 4 octets
            print("Sequence", sequence);
        }

        String nbOutputs = trx.substring(pos, pos+=2); // 1 octet
        print("Nb of Outputs", nbOutputs);

        for (int i=0 ; i<Integer.valueOf(nbOutputs) ; i++) {
            System.out.println("OUTPUT " + (i+1));
            String txValue = trx.substring(pos, pos+=16); // 8 octets
            txValue = Exercice142.convertEndian(txValue); // prix en Little Endian
            BigInteger prix = new BigInteger(txValue, 16);
            System.out.println("Prix: " + prix + " satochis");

            double d = prix.doubleValue()*0.00000001;
            double roundOff = Math.round(d * 100) / 100.00;
            System.out.println("Prix: " + roundOff + " btc");

            VarInt tailleScript = new VarInt(trx.substring(pos, pos+18)); // +18 taille max varint
            print("Locking script length", tailleScript.value);

            String pubKey = trx.substring(pos+=tailleScript.length*2, pos+=tailleScript.value*2);
            print("Locking script", pubKey);
        }

        System.out.println("");
        String lockTime = trx.substring(pos, pos = pos+8); // 4 octets
        print("lockTime", lockTime);
    }

    private static void print(String label, int value) {
        System.out.println(label + ": " + value);
    }

    private static void print(String label, String value) {
        System.out.println(label + ": " + value + "  " + value.length()/2 + " octets");
    }
}
