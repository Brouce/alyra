import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Hex;

import java.math.BigInteger;

public class Exercice1414 {

    /** sources:
     * http://learnmeabitcoin.com/glossary/block-header
     * http://learnmeabitcoin.com/glossary/bits
     */


    public Exercice1414() {

    }


    private boolean cibleAtteinte(String coeff, int exposant, String hash) {

        // target est sur 32 octets
        String target = coeff;
        target = StringUtils.rightPad(target, exposant*2, "0");
        target = StringUtils.leftPad(target, 64, "0");

        System.out.println("target: " + target);
        System.out.println("hash:   " + hash);

        if (new BigInteger(hash,16).compareTo(new BigInteger(target,16)) == -1) return true;
        else return false;
    }

    public boolean validationPOW(String block) {

        // supprime "0x"
        block = block.substring(2);

        String header = block.substring(0, 160); // 80 octets du header

        String exposant = header.substring(75*2, 76*2); // 1 octet

        String coeff = header.substring(72*2, 75*2); // 3 octets
        coeff = Utils.convertEndian(coeff); // Little Endian

        // double sha256 sur le header
        byte[] sha256 = DigestUtils.sha256(header.getBytes());
        sha256 = DigestUtils.sha256(sha256);
        String sha256hex = Hex.toHexString(sha256);
        sha256hex = Utils.convertEndian(sha256hex);// Little Endian

        return cibleAtteinte(coeff, Integer.valueOf(exposant, 16), sha256hex);
    }


    public static void main(String[] args) {

        Exercice1414 exo = new Exercice1414();

        boolean test = exo.validationPOW("0x01000000008de6ae7a37b4f26a763f4d65c5bc7feb1ad9e3ce0fff4190c067f0000000000913281d"+
        "b730c5cff987146330508c88cc3e642d1b9f5154854764fd547e0a54eaf26849ffff001d2e4a4c3d010100000001000000"+
        "0000000000000000000000000000000000000000000000000000000000ffffffff0704ffff001d013fffffffff0100f205"+
        "2a010000004341041ada81ea00c11098d2f52c20d5aa9f5ba13f9b583fda66f2a478dd7d95a7ab615159d98b63df2e6f3e"+
        "cb3ef9eda138e4587e7afd31e7f434cbb6837e17feb0c5ac00000000");

        System.out.println(test ? "Block confirmé !" : "Block KO");
    }
}