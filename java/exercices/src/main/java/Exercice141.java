public class Exercice141 {

    // Exercice 1.4.1 : Convertir un nombre décimal en hexadécimal

    private static char hex[]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

    public static void main(String[] args) {

        System.out.println("Exercice 1.4.1 : Convertir un nombre décimal en hexadécimal");

        Integer num = 466321;
        System.out.println(num);
        System.out.println("Big Endian:    " + toHexBigEndian(num));
        System.out.println("Little Endian: " + toHexLittleEndian(num));
    }

    private static String toHexBigEndian(Integer num) {
        String out = "";
        int rem ; // reste

        while(num>0){
            rem = num%16;
            out = hex[rem] + out; // ajoute par la gauche
            num = num/16;
        }

        // complète avec un zéro pour avoir un nombre pair d'hexa
        if (out.length()%2 != 0) out = "0" + out;

        return out;
    }

    // les hexa marchent par paire de 2 (= 1 octet), qui doivent rester dans l'ordre
    private static String toHexLittleEndian(Integer num) {
        String hexBE = toHexBigEndian(num);
        String out = "";

        // retourne les hexa en conservant les paires
        for (int i = hexBE.length() - 2; i >= 0; i -= 2) {
            out += hexBE.substring(i, i + 2);
        }

        return out;
    }
}
