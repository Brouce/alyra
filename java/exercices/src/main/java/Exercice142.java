public class Exercice142 {

    // Exercice 1.4.2 : Convertir un nombre en little endian

    private static char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static void main(String[] args) {

        System.out.println("Exercice 1.4.2 : Convertir un nombre en little endian");

        Integer num = 643;
        System.out.println(num);
        System.out.println("Big Endian:    " + toHexBigEndian(num));
        System.out.println("Little Endian: " + toHexLittleEndian(num));
        System.out.println("Little Endian variable: " + toVarInt(num));
    }

    /**
     * Préfixe:
     * FD -> 2 octets
     * FE -> 4 octets
     * FF -> 8 octets
     */
    public static String toVarInt(Integer num) {

        String hex = toHexLittleEndian(num);

        if (num < 253) return hex; // 1 octet jusqu'a 0xFC

        else {

            // 0xFD + 2hex
            // complété pour FD, FE et FF
            if (hex.length() >= 2 && hex.length() <= 4) {
                char[] out = {'F', 'D', '0', '0', '0', '0'};
                for (int i = 0; i < hex.length(); i++) out[i + 2] = hex.charAt(i);
                return String.valueOf(out);
            }

            // 0xFE + 4hex
            else if (hex.length() >= 6 || hex.length() <= 8) {
                char[] out = {'F', 'E', '0', '0', '0', '0', '0', '0', '0', '0'};
                for (int i = 0; i < hex.length(); i++) out[i + 2] = hex.charAt(i);
                return String.valueOf(out);
            }

            // 0xFF + 8hex
            else if (hex.length() >= 10 || hex.length() <= 16) {
                char[] out = {'F', 'F', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};
                for (int i = 0; i < hex.length(); i++) out[i + 2] = hex.charAt(i);
                return String.valueOf(out);
            }

            System.err.println("Erreur");
            return null;
        }
    }

    public static String toHexBigEndian(Integer num) {
        String out = "";
        int rem; // reste

        while (num > 0) {
            rem = num%16;
            out = hex[rem] + out; // ajoute par la gauche
            num = num/16;
        }

        // complète avec un zéro pour avoir un nombre pair d'hexa
        if (out.length() % 2 != 0) out = "0" + out;

        return out;
    }

    // les hexa marchent par paire de 2 (= 1 octet), qui doivent rester dans l'ordre
    public static String toHexLittleEndian(Integer num) {
        String hexBE = toHexBigEndian(num);
        String out = "";

        // retourne les hexa en conservant l'ordre des paires
        for (int i = hexBE.length() - 2; i >= 0; i -= 2) {
            out += hexBE.substring(i, i + 2);
        }

        return out;
    }

    /**
     * Converti dans les 2 sens
     * little endian -> big endian et
     * big endian -> little endian
     */
    public static String convertEndian(String hex) {
        if (hex.length()%2 != 0) throw new RuntimeException(hex + " devrait avoir une longueur paire");

        String out = "";

        // retourne les hexa en conservant l'ordre des paires
        for (int i = hex.length() - 2; i >= 0; i -= 2) {
            out += hex.substring(i, i + 2);
        }

        return out;
    }
}
