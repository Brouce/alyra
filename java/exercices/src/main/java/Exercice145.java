import java.util.*;

/**
 * Exercice 1.4.5 : Écrire une calculatrice en polonaise inversée
 */
public class Exercice145 {

    private final static String chiffres = "0123456789";
    private final static String opérateurs = "+-*/";
    private final static String comparateurs = "=<>";

    /**
     * Le séparateur est un espace
     * @param in la chaine en notation polonaise inversée
     */
    private static String calculer(String in){

        // sépare les arguments dans un tokenizer
        StringTokenizer args = new StringTokenizer(in, " ");

        // pile tampon qui varie au cours des opérations
        Deque<Double> stack = new LinkedList<>();

        double arg1, arg2; // pour les opérandes
        String arg3; // pourles opérateurs ou comparateurs

        do {
            arg3 = args.nextToken();
            if (!comparateurs.contains(arg3) && !opérateurs.contains(arg3)) {
                // c'est un nombre, on l'ajoute à la pile
                stack.push(Double.valueOf(arg3));
                continue;
            }

            else if (opérateurs.contains(arg3)) {
                // c'est un opérateur, il doit y avoir 2 entrée minimum au dessus de la pile
                double result;

                // prend deux opérandes dans la pile
                arg2 = stack.pop();
                arg1 = stack.pop();

                if (arg3.equals("+")) result = arg1 + arg2;
                else if (arg3.equals("-")) result = arg1 - arg2;
                else if (arg3.equals("*")) result = arg1 * arg2;
                else if (arg3.equals("/")) result = arg1 / arg2;
                else throw new RuntimeException("Erreur");

                // remet le résultat au dessus de la pile
                stack.push(result);
            }

            else if (comparateurs.contains(arg3)) {
                // c'est un comparateur, en dernière position

                // prend deux opérandes dans la pile
                arg2 = stack.pop();
                arg1 = stack.pop();

                if (arg3.equals(">")) return arg1 > arg2 ? "true" : "false";
                else if (arg3.equals("<")) return arg1 < arg2 ? "true" : "false";
                else if (arg3.equals("=")) return arg1 == arg2 ? "true" : "false";
                else throw new RuntimeException("Erreur");
            }

        } while (args.hasMoreTokens());

        return String.valueOf(stack.pop());
    }


    public static void main(String[] args){

        System.out.println("Exercice 1.4.5 : Écrire une calculatrice en polonaise inversée");

        String op = "10 2 -";
        System.out.println("\"" + op + "\"   -> " + calculer(op));

        op = "1 2 + 4 * 3 +";
        System.out.println("\"" + op + "\"   -> " + calculer(op));

        op = "3 4 1 2 + * + 15 =";
        System.out.println("\"" + op + "\"   -> " + calculer(op));
    }
}