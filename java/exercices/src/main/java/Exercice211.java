import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Hex;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;

public class Exercice211 {

    /** sources:
     */

    // 2^{224} - 1
    // The maximum target used by SHA256 mining devices
    // 00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffff
    private static final BigInteger maxCible1 = (new BigInteger("2").pow(224)).subtract(BigInteger.ONE);

    // (2¹⁶ - 1) * 2²⁰⁸
    // Because Bitcoin stores the target as a floating-point type, this is truncated:
    // 00000000ffff0000000000000000000000000000000000000000000000000000
    private static final BigInteger maxCible = (new BigInteger("2").pow(16).subtract(BigInteger.ONE))
            .multiply(new BigInteger("2").pow(208));


    public Exercice211() {
        System.out.println("Cible1 : " + StringUtils.leftPad(Hex.toHexString(maxCible1.toByteArray()), 64, "0"));
        System.out.println("Cible  : " + StringUtils.leftPad(Hex.toHexString(maxCible.toByteArray()), 64, "0"));

        System.out.println("");
    }

    private void printDifficulté(String hex) {
        String target = convertTargetHexToStringHex(hex);
        BigInteger bigint = convertTargetStringToBigInt(target);
        Float f = calculerDifficulte(bigint);
        System.out.println(" difficulté de " + hex + " target: " + target + ": " + f);
    }

    private Float calculerDifficulte(BigInteger cible) {

        BigInteger difficulté = maxCible.divide(cible);

        BigDecimal m = new BigDecimal(maxCible);
        BigDecimal c = new BigDecimal(cible);
        BigDecimal test = m.divide(c, 2, RoundingMode.CEILING);

        return test.floatValue();
    }

    /**
     * @param hex sous la forme 0xEETTTTTT
     *            EE exposant 1 octet
     *            TTTTTT target 3 octets
     */
    private static String convertTargetHexToStringHex(String hex) {
        // target est sur 32 octets
        String target = hex.substring(4, 10);
        int exposant = Integer.valueOf(hex.substring(2,4), 16);
        target = StringUtils.rightPad(target, exposant * 2, "0");
        target = StringUtils.leftPad(target, 64, "0");
        return target;
    }

    private static BigInteger convertTargetStringToBigInt(String target) {
        BigInteger out = new BigInteger(target,16);
        return out;
    }

    private boolean blocReajustement(int blockHeight) {

        boolean out;
        if (blockHeight % 2016 == 0) out = true;
        else out = false;

        return out;
    }

    /**
     * Cible = CiblePrécédente x ( bloc[Actuel].date - block[Actuel-2016].date ) /(deux semaines en secondes)
     */
    private BigInteger calculerCible(int date1, int date2, BigInteger previousTarget) {

        Integer duration = date1 - date2; // secondes
        System.out.println("Durée: " + duration/60/60/24 + " jours");

        // en secondes
        Integer deuxSemaines = 60*60*24*2;

        BigInteger nexTarget = previousTarget.multiply(new BigInteger(duration.toString())
                .divide(new BigInteger(deuxSemaines.toString())));

        return nexTarget;
    }



    private void blockInfo(String blockHash) {

        try {
            // get the JSON from the block
            String adresse = "https://blockchain.info/rawblock/" + blockHash;
            System.out.println("Adresse: " + adresse);

            // extract infos from Json String
            JSONObject json = new JSONObject(getJsonString(adresse));
            String hash = json.getString("hash");
            int blockHeight = json.getInt("height");
            int time = json.getInt("time");
            int bits = json.getInt("bits");
            String bitsHex = "0x" + Hex.toHexString(BigInteger.valueOf(bits).toByteArray());
            BigInteger bitsInt = new BigInteger(bitsHex.substring(2), 16);

            System.out.println("hash: " + hash + " blockHeight: " + blockHeight + " time: "
                    + time + " bits: " + bits + " -> " + bitsHex);
            printDifficulté(bitsHex);

            boolean test3 = this.blocReajustement(blockHeight);
            System.out.println("Réajustement pour le block #" + blockHeight + " ?  " + test3);


            // cherche le block de réajustement précédent
            int previousBlockAjustement = blockHeight -1;
            while ( !blocReajustement(previousBlockAjustement) ) {
                previousBlockAjustement--;
            }

            // nombre de block depuis le dernier ajustement
            int nbBlocks = blockHeight - previousBlockAjustement;

            // avec 1 block toutes les 10 minutes, nb de secondes jusqu'au block de réajustement précédent
            int nbSec = nbBlocks * 10*60; // secondes
            System.out.println("block de réajustement précédent: #" + previousBlockAjustement + " , " + nbSec/60/60/24 + " jours plus tôt");

            // cherche les données du block de réajustement précedent
            adresse = "https://blockchain.info/block-height/" + previousBlockAjustement + "?format=json";
            System.out.println("Adresse: " + adresse);

            // extract infos from Json String
            json = new JSONObject(getJsonString(adresse));
            JSONObject jsonZero = json.getJSONArray("blocks").getJSONObject(0); // prend le block "0" dans la liste
            String hashPB = jsonZero.getString("hash");
            int blockHeightPB = jsonZero.getInt("height");
            int timePB = jsonZero.getInt("time");
            int bitsPB = jsonZero.getInt("bits");
            String bitsHexPB = "0x" + Hex.toHexString(BigInteger.valueOf(bitsPB).toByteArray());
            BigInteger bitsIntPB = new BigInteger(bitsHex.substring(2), 16);
            System.out.println("hash: " + hashPB + " blockHeight: " + blockHeightPB + " time: "
                    + timePB + " bits: " + bitsPB + " -> " + bitsHexPB);

            Integer timDiff = time - timePB; // temps (sec) entre le block courant et le block de réajustment précédent
            Integer timeDiffThéo = (blockHeight - blockHeightPB) * 10*60; // pareil mais compté en nombre de blocks, avec 1 block / 10 minutes
            Float variation = bitsInt.floatValue() * timDiff/timeDiffThéo;

            System.out.println("Cible courante pour le block  #" + blockHeight + ": " + bitsInt);
            System.out.println("Cible théorique pour le block #" + blockHeight + ": " + Math.round(variation));

            // cherche le block de réajustement suivant
            int nextBlockAjustement = previousBlockAjustement +1;
            while ( !blocReajustement(nextBlockAjustement) ) {
                nextBlockAjustement++;
            }

            String test = bitsInt.floatValue() > variation ? "augmenter":"diminuer";
            System.out.println("A priori la difficulté du block de réajustement suivant #" + nextBlockAjustement
                    + " devrait " + test);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {

        // block n°557098 Jan 5, 2019 5:03:57 AM
        // block hash: 00000000000000000019b2634066a100e56ed58a0ae40ca5a4e2d1dba6a4be22
        // Merkle root: fc7049836a87f0cce350375e3a72f5a43b1e6b8907d946573793d7d7f06e8e5a
        // bits: 389159077 -> 0x173218a5
        // bits 173218a5 = The target in a compact format , target sur 32 bytes,  17=nb de zéros par la droite, coef 3218a5

        Exercice211 exo = new Exercice211();
        BigInteger target = new BigInteger("1147152896345386682952518188670047452875537662186691235300769792000");

        Float test1 =  exo.calculerDifficulte(target);
        System.out.println("Difficulté de " + target + ": " + test1);


        exo.printDifficulté("0x1d00ffff"); // max difficulty
        exo.printDifficulté("0x1c0ae493");
        exo.printDifficulté("0x173218a5");
        exo.printDifficulté("0x1b0404cb");


        System.out.println("");
        exo.blocReajustement(556415);
        exo.blocReajustement(556416);


        // exercice optionnel 2.1.4
        System.out.println("");
        System.out.println("Exercice 2.1.4 optionnel, partie 1");
        target = convertTargetStringToBigInt(convertTargetHexToStringHex("0x173218a5"));
        BigInteger nextTarget = exo.calculerCible(1545175965,1543838368, target);
        System.out.println("Cible            " + StringUtils.leftPad( Hex.toHexString(target.toByteArray()), 64, "0") + " difficulté " + exo.calculerDifficulte(target));
        System.out.println("Cible suivante   " + StringUtils.leftPad( Hex.toHexString(nextTarget.toByteArray()), 64, "0")  + " difficulté " + exo.calculerDifficulte(nextTarget));

        // Bloc #565486
        System.out.println("");
        System.out.println("Exercice 2.1.4 optionnel, partie 2");
        exo.blockInfo("0000000000000000001cdfcd0f8039fbb891de53c064a2c21c531db7d63a6fab");
    }


    /**
     * @return un JSON depuis un block bitcoin
     */
    private String getJsonString(String adresse) throws IOException {
        URL url = new URL(adresse);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer rawData = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            rawData.append(inputLine);
            //System.out.println("rawData: " + inputLine);
        }
        in.close();
        con.disconnect();

        return rawData.toString();
    }
}