import org.bouncycastle.util.encoders.Hex;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalTime;

public class Exercice1411 {

    /* sources:
    */

    private SecureRandom rnd;
    private byte[] alea;

    /**
     * init random
     */
    public Exercice1411() {
        try {
            rnd = SecureRandom.getInstance("SHA1PRNG","SUN");
        } catch (Exception e) {
            System.out.println(e.getMessage());

            // si pas trouvé, init par defaut de SecureRandom sans provider/algo
            rnd = new SecureRandom();
        }
    }


    private void rechercheDebut(String prefix, int size) {

        prefix = prefix.toLowerCase();

        // init de l'aléa avec la taile demandée
        alea = new byte[size];
        LocalTime begin = LocalTime.now();

        BigInteger count = BigInteger.ZERO;

        do {
            rnd.nextBytes(alea); // rempli alea avec bes bytes fournis par le random
            count = count.add(BigInteger.ONE);
        } while ( !Hex.toHexString(alea).startsWith(prefix) );

        // durée de l'opération
        LocalTime end = LocalTime.now();
        Duration duration = Duration.between(begin, end);

        System.out.println("Prefix demandé: " + prefix + " Alea: " + Hex.toHexString(alea) + " count: " + count
                + " durée: " + duration);
    }


    public static void main(String[] args) {

        // tous les indices représentent des octets

        Exercice1411 exo = new Exercice1411();

        for (int tailleAlea=1 ; tailleAlea<=4 ; tailleAlea++) {

            System.out.println("Taille Alea demandé: " + tailleAlea + " octets");

            for (int taillePrefix=1 ; taillePrefix <= tailleAlea ; taillePrefix++) {

                // rempli le préfixe avec le nb d'octets demandés
                byte préfix[] = new byte[taillePrefix];
                exo.rechercheDebut(Hex.toHexString(préfix), tailleAlea);
            }
        }
    }
}