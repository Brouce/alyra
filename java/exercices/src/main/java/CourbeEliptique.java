import org.bouncycastle.util.BigIntegers;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Implémentation pour Secp256k1
 */
public class CourbeEliptique  implements Comparable {

    private BigInteger a, b;
    private static BigInteger _4 = new BigInteger("4");
    private static BigInteger _27 = new BigInteger("27");

    // constructeur
    public CourbeEliptique(BigInteger aa, BigInteger bb) {

        // limite de la courbe
        if (_4.multiply(aa.pow(3)).add(_27.multiply(bb.pow(2))) == BigInteger.ZERO) {
            System.out.println("Courbe donnée invalide");
        } else {
            System.out.println("Nouvelle courbe elliptique: y^2 = x^3 +" + aa + "x +" + bb);
            this.a = aa;
            this.b = bb;
        }
    }

    public int compareTo(Object o) {
        CourbeEliptique comp = (CourbeEliptique) o;
        if (this.a == comp.a && this.b == comp.b)
            return 0;
        else return 1;
    }

    /**
     * @return true si y2 = x3 + ax +b
     */
    public void testPoint(BigInteger x, BigInteger y) {
        System.out.println(y.pow(2));
        System.out.println(x.pow(3).add( (this.a.multiply(x)) ).add(this.b) );
        if ( y.pow(2).equals(x.pow(3).add( (this.a.multiply(x)) ).add(this.b) ) )
            System.out.println("(" + x + "," + y + ") appartient à la courbe");
        else System.out.println("(" + x + "," + y + ") n'appartient pas à la courbe");
    }

    public String toString() {
        return "y^2 = x^3 + " + this.a + "x +" + this.b;
    }
}
