import org.apache.commons.codec.digest.DigestUtils;
import org.bitcoinj.core.Base58;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.util.encoders.Hex;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Sources:
 * http://lenschulwitz.com/base58
 * https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses
 */
public class Exercice136 {


    // https://en.bitcoin.it/wiki/Secp256k1

    // G est un point de la courbe
    public static final BigInteger Gx = new BigInteger("79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798", 16);
    public static final BigInteger Gy = new BigInteger("483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8", 16);

    // clé privée: choisir au hasard un nombre entre 1 et n-1
    public static final BigInteger n =
            new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141", 16);


    public static void main(String[] args) {

        try {
            System.out.println("Exercice 1.3.6 : Génèrer une clé publique pour Bitcoin");

            ECKeyPair ec = ECKeyPair.createNew(false);
            System.out.println("BC Private key: " + ec.priv);
            System.out.println("BC public key: " + Hex.toHexString(ec.pub));

            // 1: clé privée
            byte[] privKey = genererPrivateKey();
            System.out.println("");

            // 2: clé publique depuis la clé privée
            //testWeb3j(BigIntegers.fromUnsignedByteArray(privKey, 0, 32));
            byte[] publicKey = genererPublicKey(privKey);

            // 3: adresse BTC depuis la clé publique
            genererAdresseBtc(publicKey);


        } catch (RuntimeException re) {
            System.err.println(re.getMessage());
        }
    }

    public static void checkG() {
        CourbeEliptique ce = new CourbeEliptique(BigInteger.ZERO, BigInteger.valueOf(7));
        ce.testPoint(Gx, Gy);
    }


    /**
     * Fabrique une adressse Bitcoin à partir d'une clé publique
     *
     * @return l'adresse en base 58
     */
    private static String genererAdresseBtc(byte[] publicKey) {

        // calcule un hash sha256, 32 octets
        byte[] sha256 = DigestUtils.sha256(publicKey);
        System.out.println("sha256: " + Hex.toHexString(sha256) + " length: " + sha256.length + " octets");

        // calcule un hash ripemp160, 20 octets
        RIPEMD160Digest hash20 = new RIPEMD160Digest();
        hash20.update(sha256, 0, sha256.length);
        byte[] rpd160 = new byte[hash20.getDigestSize()];
        hash20.doFinal(rpd160, 0);
        System.out.println("ripemd160: " + Hex.toHexString(rpd160) + " length: " + rpd160.length + " octets");


        // Ajoute le préfixe '00' à l'adresse
        byte[] addrTmp = new byte[rpd160.length + 1]; // 21 octets
        addrTmp[0] = 0x00; // préfix de 1 byte 0x00
        System.arraycopy(rpd160, 0, addrTmp, 1, rpd160.length); // reste de l'adresse
        System.out.println("Adresse temp: " + Hex.toHexString(addrTmp) + " length: " + addrTmp.length + " octets");

        // calcule un double hash de l'adresse précédente
        byte[] doubleHash = DigestUtils.sha256(addrTmp);
        doubleHash = DigestUtils.sha256(doubleHash);
        System.out.println("doubleHash: " + Hex.toHexString(doubleHash));

        // 4 premiers octets de doubleHash à la fin de l'adresse BTC
        byte[] addrBtc = new byte[addrTmp.length + 4]; // 25 octets
        System.arraycopy(addrTmp, 0, addrBtc, 0, 21);  // copie de l'adresse en 0-21
        System.arraycopy(doubleHash, 0, addrBtc, 21, 4); // copie des 4 derniers octets
        System.out.println("Adresse btc: " + Hex.toHexString(addrBtc) + " length: " + addrBtc.length + " octets");

        // Converti en base58
        String addrBtcB58 = Base58.encode(addrBtc);
        System.out.println("Adresse btc en base 58: " + addrBtcB58 + " length: " + addrBtcB58.length());

        return addrBtcB58;
    }

    // clé_publique = clé_privée * G (modulo n)
    public static byte[] genererPublicKey(byte[] privateKey) {

        return null;
    }



    public static String compressPubKey(BigInteger pubKey) {
        String pubKeyYPrefix = pubKey.testBit(0) ? "03" : "02";
        String pubKeyHex = pubKey.toString(16);
        String pubKeyX = pubKeyHex.substring(0, 64);

        String compPubKey = pubKeyYPrefix + pubKey;
        System.out.println("compressed PubKey: " + compPubKey + " length " + compPubKey.length());

        return compPubKey;
    }

    public static void testWeb3j(BigInteger privKey) {

        try {
            /*
            //BigInteger privKey = Keys.createEcKeyPair().getPrivateKey();
            //BigInteger privKey = new BigInteger("97ddae0f3a25b92268175400149d65d6887b9cefaf28ea2c078e05cdc15a3c0a", 16);
            BigInteger pubKey = Sign.publicKeyFromPrivate(privKey);
            ECKeyPair keyPair = new ECKeyPair(privKey, pubKey);
            System.out.println("web3j Public key: " + pubKey.toString(16) + " length " + pubKey.toByteArray().length + " bytes");

            //compressPubKey(pubKey);


            String msg = "Message for signing";
            byte[] msgHash = Hash.sha3(msg.getBytes());
            Sign.SignatureData signature = Sign.signMessage(msgHash, keyPair, false);
            System.out.println("Msg: " + msg);
            System.out.println("Msg hash: " + Hex.toHexString(msgHash));
            System.out.printf("Signature: [v = %d, r = %s, s = %s]\n",
                    signature.getV() - 27,
                    Hex.toHexString(signature.getR()),
                    Hex.toHexString(signature.getS()));

            System.out.println();


            BigInteger pubKeyRecovered = Sign.signedMessageToKey(msg.getBytes(), signature);
            System.out.println("Recovered public key: " + pubKeyRecovered.toString(16));

            boolean validSig = pubKey.equals(pubKeyRecovered);
            System.out.println("Signature valid? " + validSig);
            SignatureException
            */
        } catch (RuntimeException  re) {
            System.err.println(re);

        }
    }

    /**
     * A random private key that can be used with Secp256k1.
     */
    public static byte[] genererPrivateKey() {
        SecureRandom rnd;
        try {
            rnd = SecureRandom.getInstance("SHA1PRNG","SUN");
        } catch (Exception e) {
            System.out.println(e.getMessage());

            // si pas trouvé, init par defaut de SecureRandom sans provider/algo
            rnd = new SecureRandom();
        }

        // Generate the key, skipping as many as desired.
        byte[] pk = new byte[32]; // pk de 256 bit
        rnd.nextBytes(pk);

        BigInteger pkCheck = new BigInteger(1, pk);

        while (pkCheck.compareTo(BigInteger.ZERO) == 0
                || pkCheck.compareTo(n) == 1) {
            rnd.nextBytes(pk);
            pkCheck = new BigInteger(1, pk);
        }


        System.out.println("Private key: " + Hex.toHexString(pk));
        return pk;
    }
}
