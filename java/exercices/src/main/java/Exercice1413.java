import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;

public class Exercice1413 {

    /** sources:
     * http://learnmeabitcoin.com/glossary/block-header
     * http://learnmeabitcoin.com/glossary/bits
     */


    public Exercice1413() {

    }


    private boolean cibleAtteinte(String coeff, int exposant, String hash) {

        // target est sur 32 octets
        String target = coeff;
        target = StringUtils.rightPad(target, exposant*2, "0");
        target = StringUtils.leftPad(target, 64, "0");

        System.out.println(target);
        System.out.println(hash);

        if (new BigInteger(hash,16).compareTo(new BigInteger(target,16)) == -1) return true;
        else return false;
    }


    public static void main(String[] args) {

        // block n°557098 Jan 5, 2019 5:03:57 AM
        // block hash: 00000000000000000019b2634066a100e56ed58a0ae40ca5a4e2d1dba6a4be22
        // Merkle root: fc7049836a87f0cce350375e3a72f5a43b1e6b8907d946573793d7d7f06e8e5a
        // bits 173218a5 = The target in a compact format , target sur 32 bytes,  17=nb de zéros par la droite, coef 3218a5

        Exercice1413 exo = new Exercice1413();
        boolean test = exo.cibleAtteinte("3218a5", 23, "00000000000000000019b2634066a100e56ed58a0ae40ca5a4e2d1dba6a4be22");
        System.out.println(test ? "Cible atteinte !" : "Hash trop grand");
        System.out.println("");

        test = exo.cibleAtteinte("3218a5", 23, "0000000000000ff00019b2634066a100e56ed58a0ae40ca5a4e2d1dba6a4be22");
        System.out.println(test ? "Cible atteinte !" : "Hash trop grand");
    }
}