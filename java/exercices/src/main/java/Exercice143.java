import java.math.BigInteger;

public class Exercice143 {

    // Exercice 1.4.3 : Identifier les différents champs d’une transaction

    public static void main(String[] args) {

        System.out.println("Exercice 1.4.3 : Identifier les différents champs d’une transaction");

        String trx = "1941e985075825e09de53b08cdd346bb67075ef0ce5c94f98853292d4bf94c10d010000006b483045022100ab44ef4" +
                "25e6d85c03cf301bc16465e3176b55bba9727706819eaf07cf84cf52d02203f7dc7ae9ab36bead14dd3c83c8c030bf8" +
                "ce596e692021b66441b39b4b35e64e012102f63ae3eba460a8ed1be568b0c9a6c947abe9f079bcf861a7fdb2fd577ed" +
                "48a81Feffffff";

        decodeTrx(trx);
    }

    /**
     * Préfixe varint:
     * FD -> 2 octets
     * FE -> 4 octets
     * FF -> 8 octets
     *
     * @return la valeur du varint
     */
    public static int fromVarInt(String varInt) {
        if (varInt.substring(0,2).equals("fd")) return Integer.valueOf(varInt.substring(2,6), 16); // 2 octets
        else if (varInt.substring(0,2).equals("fe")) return Integer.valueOf(varInt.substring(2,10), 16); // 4 octets
        else if (varInt.substring(0,2).equals("ff")) return Integer.valueOf(varInt.substring(2,18), 16); // 8 octets
        else return Integer.valueOf(varInt.substring(0,2),16); // varint < 253
    }

     /* Décomposition de la transaction:
     * - 1 number of transaction inputs
     * - 32 hash trx précedente
     * - 4 index trx
     * - ScriptSig:
     *      - varInt ScriptSig length
     *      - varInt taille signature
     *      - signature
     *      - varInt taille clé publique
     *      - clé publique
     * - 4 sequence 0xffffffff
     */
    private static void decodeTrx(String trx) {

        /*
        01
        941e9850 75825e09 de53b08c dd346bb6 7075ef0c e5c94f98 853292d4 bf94c10d
        01000000
        6b
        48
        3045022100ab44ef425e6d85c03cf301bc16465e3176b55bba9727706819eaf07cf84cf52d02203f7dc7ae9ab36bead14dd3c83c8c030bf8ce596e692021b66441b39b4b35e64e01
        21
        02f63ae3eba460a8ed1be568b0c9a6c947abe9f079bcf861a7fdb2fd577ed48a81
        Feffffff
         */

        trx = trx.toLowerCase(); // piège ?

        // corrige la taille de la trx: lenght trx ko si sa taille est impaire
        if (trx.length()%2 != 0) trx = "0" + trx;
        System.out.println("trx length: " + trx.length()/2 + " octets");

        String nbOfTrxInputs = trx.substring(0,2); // 1 octet
        System.out.println("Nb de trx en entrée: " + nbOfTrxInputs);

        String hashPreviousTrx = trx.substring(2,66); // 32 octets
        System.out.println("hash trx précédente: " + hashPreviousTrx + " -> " + hashPreviousTrx.length()/2 + " octets");

        String indexTrx = trx.substring(66,74);
        String indexTrxBigEndian = Exercice142.convertEndian(indexTrx);
        System.out.println("index de la trx précédente: " + indexTrx + " -> " + new BigInteger(indexTrxBigEndian,16));

        String ssVarIntGlobal = trx.substring(74,92); // +18 pour la taille max possible d'un varint
        System.out.println("ssVarIntGlobal: " + fromVarInt(ssVarIntGlobal) + " octets");
        String scriptSig = trx.substring(76, 76+fromVarInt(ssVarIntGlobal)*2);

        String ssVarIntSignature = scriptSig.substring(0,18); // +18
        System.out.println("- varInt signature: " + fromVarInt(ssVarIntSignature) + " octets");

        String ssSignature = scriptSig.substring(2, 2+fromVarInt(ssVarIntSignature)*2);
        System.out.println("- signature: " + ssSignature + " -> " + ssSignature.length()/2 + " octets");

        String ssVarIntPubKey = scriptSig.substring(2+fromVarInt(ssVarIntSignature)*2, 2+fromVarInt(ssVarIntSignature)*2+18);
        System.out.println("- varInt clé publique: " + fromVarInt(ssVarIntPubKey) + " octets");

        String ssPubKey = scriptSig.substring(4+fromVarInt(ssVarIntSignature)*2, 4+fromVarInt(ssVarIntSignature)*2+fromVarInt(ssVarIntPubKey)*2);
        System.out.println("- clé publique: " + ssPubKey + " -> " + ssPubKey.length()/2 + " octets");

        String seq = trx.substring(76+scriptSig.length(), 76+scriptSig.length()+8);
        System.out.println("Séquence: " + seq + " -> " + seq.length()/2 + " octets");
    }
}
