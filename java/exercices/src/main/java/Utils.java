/**
 * Utilitaires pour les cours Alyra
 */
public class Utils {

    private static char hex[]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

    // trim les blancs
    public static String trim(String texte) {
        if (texte.length() == 0) return "";
        else {
            String out = "";
            char blanc = ' ';
            for (int i = 0; i < texte.length(); i++) {
                out += texte.charAt(i) == blanc ? "" : texte.charAt(i);
            }
            return out;
        }
    }

    private static String toHexBigEndian(Integer num) {
        String out = "";
        int rem ; // reste

        while(num>0){
            rem = num%16;
            out = hex[rem] + out; // ajoute par la gauche
            num = num/16;
        }

        // complète avec un zéro pour avoir un nombre pair d'hexa
        if (out.length()%2 != 0) out = "0" + out;

        return out;
    }

    // les hexa marchent par paire de 2 (= 1 octet), qui doivent rester dans l'ordre
    private static String toHexLittleEndian(Integer num) {
        String hexBE = toHexBigEndian(num);
        String out = "";

        // retourne les hexa en conservant les paires
        for (int i = hexBE.length() - 2; i >= 0; i -= 2) {
            out += hexBE.substring(i, i + 2);
        }

        return out;
    }


    /**
     * Converti dans les 2 sens
     * little endian -> big endian et
     * big endian -> little endian
     */
    public static String convertEndian(String hex) {
        if (hex.length()%2 != 0) throw new RuntimeException(hex + " devrait avoir une longueur paire");

        String out = "";

        // retourne les hexa en conservant l'ordre des paires
        for (int i = hex.length() - 2; i >= 0; i -= 2) {
            out += hex.substring(i, i + 2);
        }

        return out;
    }
}
