import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Bouncy Castle
 * Exemple pour générer des clés
 */
public class ECKeyPair {

    private static final SecureRandom secureRandom = new SecureRandom();
    private static final X9ECParameters curve = SECNamedCurves.getByName("secp256k1");
    private static final ECDomainParameters domain = new ECDomainParameters(curve.getCurve(), curve.getG(), curve.getN(), curve.getH());

    public BigInteger priv;
    public byte[] pub;
    private boolean compressed;


    public static ECKeyPair createNew(boolean compressed) {

        ECKeyPairGenerator generator = new ECKeyPairGenerator();
        ECKeyGenerationParameters keygenParams = new ECKeyGenerationParameters(domain, secureRandom);
        generator.init(keygenParams);
        AsymmetricCipherKeyPair keypair = generator.generateKeyPair();
        ECPrivateKeyParameters privParams = (ECPrivateKeyParameters) keypair.getPrivate();
        ECPublicKeyParameters pubParams = (ECPublicKeyParameters) keypair.getPublic();
        ECKeyPair k = new ECKeyPair();
        k.priv = privParams.getD();
        k.compressed = compressed;
        if (compressed) {
            ECPoint q = pubParams.getQ();
            k.pub = new ECPoint.Fp(domain.getCurve(), q.getXCoord(), q.getYCoord(), true).getEncoded();
        } else {
            k.pub = pubParams.getQ().getEncoded();
        }
        return k;
    }


}

