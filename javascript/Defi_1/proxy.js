var express = require('express')
var request = require('request')
var cors = require('cors')
const app = express();

app.use(cors()) // enable CORS on all requests
app.options('*', cors()) 

// Constantes 
const BITCOIND_URL = 'http://192.168.1.17:8332/'; // bitcoin core serving JSON/RPC entry point
const BASICHEADER = { 'content-type': 'text/plain'}
const AUTHENTIFICATION = {'user': 'brouce','pass': '-MrsR0Z_L3F9lv2qNtizIki77mFhRTwqvzggITb8IXk='}
const LOCALPORT = 3000


// Serve static js files from defi1.html
app.use('/js', express.static('js'))

// Page d'accueil du proxy
app.get('/', (req, res) => {
  console.log("Index")
  res.sendFile("defi1.html", { root: __dirname })
});


// JSON/RPC générique sans param
app.get("/command/:c", (req, res, next) => {
  var command = req.params.c
  request(options(command, ""), function (error, response, body) {
    if (JSON.parse(body).error != undefined) res.status(404).send(JSON.parse(body).error.message.toString() ) 
    else { res.send(JSON.parse(body).result.toString()) }
  })
});

// JSON/RPC générique avec param
app.get("/command/:c/param/:p", (req, res, next) => {
  var command = req.params.c
  var param = req.params.p
  request(options(command, param), function (error, response, body) {
    if (JSON.parse(body).error != undefined) res.status(404).send(JSON.parse(body).error.message.toString() ) 
    else { res.send(JSON.parse(body).result.toString()) }
  })
});


// parametres d'une requete, merci Xavier
function options(method, params) {
  console.log("Options", method, params)
  return {
    url: BITCOIND_URL,
    method: 'POST',
    headers: BASICHEADER,
    body: '{"jsonrpc": "1.0", "id":"req", "method": "'+method+'", "params": ['+ params+'] }',
    auth: AUTHENTIFICATION
  }
}

// Cherche un block depuis son hash ou sa hauteur
app.get("/block/:p", (req, res, next) => {
  var param = req.params.p
  console.log("Request block/hash " + param);

  if (param.length == 64) {
    // récupérer un bloc à partir d'un hash de 32 octets      
    request(options("getblock", '"'+ param +'",0'), function (error, response, body) {
      if (JSON.parse(body).error != undefined) res.status(404).send(JSON.parse(body).error.message.toString() ) 
      else {
        var block = JSON.parse(body).result.toString()
        res.send({
          "hash": param,
          "block": block
        })
      }
    })    
  } else {
    // récupérer un bloc depuis une hauteur de block
    request(options("getblockhash", param), function (error, response, body) {
      console.log(body)
      if (JSON.parse(body).error != undefined) res.status(404).send(JSON.parse(body).error.message.toString() ) 
      else {
        var hash = JSON.parse(body).result.toString()
        request(options("getblock", '"'+ hash +'",0'), function (error, response, body) {
          var block = JSON.parse(body).result.toString()
          res.send({
            "hash": hash,
            "block": block
          })
        })
      }
    })

    
  }
});


// adresse de ce proxy, binding all
app.listen(LOCALPORT, '0.0.0.0', function () {

  // Check bitcoind is alive
  request(options("getbestblockhash", ""), function (error, response, body) {
    if (error) {
      console.log("Bitcoin JSON/RPC serveur introuvable ici: " + BITCOIND_URL)
      console.log("Exit")
      process.exit(1)
    } else {
      console.log("Bitcoin JSON/RPC server is up at " + BITCOIND_URL)
      console.log('CORS-enabled proxy server listening at http://127.0.0.1:3000/')
    }
  })
}) 