import "https://raw.githubusercontent.com/OpenZeppelin/openzeppelin-solidity/master/contracts/math/SafeMath.sol";

pragma solidity ^0.5.4;

contract Credibilite {
   
    using SafeMath for uint256;
    
    mapping (address => uint256) private cred;
    bytes32[] private devoirs;
   
    uint private addCred;
    
    constructor() public {
       addCred = 30;
    }


   function produireHash(string memory url) public pure returns (bytes32){
        bytes memory temp = bytes(url);
        require(temp.length != 0, "url is empty");
        
        // instructions en assembly pour convertir en bytes32
        bytes32 result;
        assembly { result := mload(add(temp, 32)) }
       
        return keccak256(abi.encodePacked(result));
   }
   
   function transfer(address destinataire, uint256 valeur) public {
       require(cred[msg.sender] > (valeur)); 
       require(cred[destinataire] != 0);
       cred[msg.sender] = cred[msg.sender].sub(valeur);
       cred[destinataire] = cred[destinataire].add(valeur);
   }
   
   function remettre(bytes32 dev) public returns (uint256) {
       devoirs.push(dev);
       cred[msg.sender] = cred[msg.sender].add(addCred);
       
       // -10 cred à chaque remise de devoir
       if (addCred > 10) addCred = addCred.sub(10);
       return devoirs.length;
   }
}
