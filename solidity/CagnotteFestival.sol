
import "https://raw.githubusercontent.com/OpenZeppelin/openzeppelin-solidity/master/contracts/math/SafeMath.sol";

pragma solidity ^0.5.4;

contract Cogere {
   
    using SafeMath for uint256;
   
    mapping (address => uint256) public organisateurs; // adresse orga => parts
    uint256 nbOrganisateurs; // nombre total d'organisateurs
   
    constructor() internal {
        organisateurs[msg.sender] = 100; // parts total
        nbOrganisateurs = 1;
    }
   
    function transfererOrga(address orga, uint256 parts) public {
        require(estOrga(msg.sender), "Vous devez être un organisateur");
        require(orga != msg.sender, "Opération interdite");
        require(parts < organisateurs[msg.sender], "Vous ne pouvez pas libérer ce nombre de parts");
        organisateurs[msg.sender] = organisateurs[msg.sender].sub(parts);
        organisateurs[orga] = parts;
        nbOrganisateurs = nbOrganisateurs.add(1);
    }
   
    function estOrga(address orga) public view returns (bool) {
        return organisateurs[orga] > 0 ? true:false;
    }
}


contract CagnotteFestival is Cogere {
   
    using SafeMath for uint256;
   
    mapping (address => bool) internal festivaliers;
   
    string[] private sponsors;
    uint256 private soldeDepenses;
    uint256 internal nbDePlacesRestantes;
   
    // controle des dépenses/jour
    uint256 private dateControleDepense;
    uint256 private montantControleDepense;
   
    uint256 internal dateFestival;
    uint256 internal dateLiquidation;
   
    constructor() public {
        organisateurs[msg.sender] = 100; // parts total
        nbOrganisateurs = 1;
        nbDePlacesRestantes = 4;
       
        dateFestival = getDateMinuit(now + 1 days);
        dateLiquidation = dateFestival + 2 weeks; // 2 weeks
    }
   

    // @return la date à minuit = sans les hh:mm:ss
    function getDateMinuit(uint date) internal pure returns(uint dateMinuit) {
        (uint year, uint month, uint day) = BokkyPooBahsDateTimeLibrary.timestampToDate(date);
        return BokkyPooBahsDateTimeLibrary.timestampFromDate(year, month, day);
    }
   
    function sponsoriser(string memory nom) public payable {
        require(msg.value >= 30 ether, "Le montant du sponsor doit être au moins de 30 Ether");
        sponsors.push(nom);
    }
   
    // Accepter les paiements anonymes
    function() external payable {}
   
    // Crédite le solde des dépenses
    function comptabiliserDepense(uint256 montant) private {
        require(montant != 0, "Le montant doit être plus grand que 0");
        soldeDepenses = soldeDepenses.add(montant);
    }
   
    function acheterTicket() public payable {
        require(msg.value >= 500 finney, "Place à 0.5 Ether minimum");
        require(now < dateFestival, "Le festival est terminé");
       
        festivaliers[msg.sender] = true;
        nbDePlacesRestantes = nbDePlacesRestantes.sub(1);
    }
   
    function placesRestantes() public view returns(uint256) {
        return nbDePlacesRestantes;
    }
   
    function payer(address payable destinataire, uint256 montant) public {
        require(estOrga(msg.sender), "Vous devez être un organisateur");
        require(destinataire != address(0), "Destinataire invalide");
        require(montant != 0, "Le montant doit être plus grand que 0");
        require(montant <= address(this).balance, "Montant trop élevé"); // interdit un déficit
        require(now < dateLiquidation, "Les comptes du festival sont fermés");
       
        // controle des dépenses max 10 ether/jour
        require(controleDepense(montant),"Le montant max journalier des dépenses est lmité à 10 ether");
       
        destinataire.transfer(montant); // peut faire un revert
    }
   
    function controleDepense(uint256 montant) internal returns(bool) {
        // cherche la date de controle
        while (dateControleDepense < now) {
            dateControleDepense = dateControleDepense.add(1 days);
            // si la date de controle à changée, réinitialise le montant de controle pour la journée
            montantControleDepense = 10 ether;
        }
        // la date de controle retenue sera un jour aprés 'now', mais c'est pas grave
       
        if (montantControleDepense.sub(montant) >= 0) {
            montantControleDepense = montantControleDepense.sub(montant);
            return true;
        }
    }
   
    // chaque organisateur peut retirer les fonds restants en proportion de ses parts
    function retirerPart() public payable {
        require(estOrga(msg.sender), "Vous devez être un organisateur");
        require(organisateurs[msg.sender] > 0, "Vous avez déja retiré vos gains");
        require(msg.sender != address(0), "Destinataire invalide");
        require(block.timestamp >= dateLiquidation, "Attendre 2 semaines aprés la fin du festival pour retirer ses parts");
       
        // retirer ses gains = balance du contrat * part%
        msg.sender.transfer( (address(this).balance).mul(organisateurs[msg.sender]).div(100) );
        organisateurs[msg.sender] = 0;
        nbOrganisateurs = nbOrganisateurs.sub(1);
       
        // terminer le contrat
        if (nbOrganisateurs == 0) selfdestruct(msg.sender);
    }
   
    function getNbOrganisateurs() public view returns(uint) {
        return nbOrganisateurs;
    }
}


import "https://raw.githubusercontent.com/bokkypoobah/BokkyPooBahsDateTimeLibrary/master/contracts/BokkyPooBahsDateTimeLibrary.sol";

contract Loto is CagnotteFestival {
   
    using SafeMath for uint256;
   
    struct Tirage {
        uint8 nombreTire;
        uint dateDuTirage;
       
        uint256 countNbTickets;
        address[] participants;
        mapping (address => uint8) participantsMap; // adresse => nombre choisi
       
        uint256 countNbTicketsGagnants;
        address[] gagnants;
       
        bool tirageTermine; // true= terminé, false= en cours (par défaut)
    }
    Tirage[] public tirages;
    mapping (uint => uint8) public indexTirage; // date => index de tirages[]
   
    constructor() public {
       
    }
   
    function acheterTicketLoto(uint8 nombre) public payable {
        require(festivaliers[msg.sender], "Vous devez être festivalier");
        require(msg.value == 100 finney, "Un ticket de loto vaut 100 finney");
        require(nombre > 0 && nombre < 256, "Vous devez choisir un nombre entre 1 et 255");
       
        uint index = getIndexProchainTirage(now);
        require(tirages[index].participantsMap[msg.sender] > 0, "Vous avez déja un ticket pour le prochain tirage");
       
        // le festivalier obtient un ticket, qui est associé à un tirage programmé (1 tirage/5 jours)
        tirages[index].countNbTickets = tirages[index].countNbTickets.add(1); // nb tickets +1
        tirages[index].participants.push(msg.sender);
        tirages[index].participantsMap[msg.sender] = nombre;
    }
   
    // @return l'index du prochain tirage aprés la date fournie, ou revert si il n'y a pas de prochain tirage prévu
    function getIndexProchainTirage(uint256 date) private returns(uint) {
       
        // le festivalier ne pourra pas acheter un ticket le jour du tirage
        uint d = getDateMinuit(date) + 1 days; // un jour de batement
       
        // incrémente d de 1 jour jusqu'a trouver le prochain jour de tirage
        while (d < dateLiquidation) {
           
            // vérifie si un tirage est prévu à cette date, sinon créé un nouveau tirage
            if ( (d.sub(dateFestival)) % 5 days == 0) {
               
                uint256 index; // index de tirages[]
                for (index=0; index < tirages.length ; index++) {
                    if ( tirages[index].dateDuTirage == d) return index;
                }
               
                // pas trouvé, on fabrique un nouveau tirage
                index = index.add(1);
                tirages[index].dateDuTirage = getDateMinuit(d);
                tirages[index].tirageTermine = false;
                return index;
            }
            d.add(1 days);
        }
        revert(); // il n'y a pas de tirage prévu pour la date donnée
    }
   
    // retire les gains de cette adresse pour tous les tirages gagnants
    function retirerGainLoto() public {
       
        uint d = getDateMinuit(dateFestival) + 5 days; // date du premier tirage
        uint256 totalGain = 0; // le festivalier a pu acheter plusieurs tickets pour des tirages différents
       
        // pour tous les tirages
        for (uint index=0; index<tirages.length ; index++) {
            assert(tirages[index].nombreTire != 0); // il devrait y avoir un nombre tiré
           
            // termine le tirage quand la date est dépassée
            if (tirages[index].dateDuTirage < now && !tirages[index].tirageTermine) {
                terminerTirage(index);
            }
           
            // quand le tirage est terminé, cherche cette adresse parmi les gagnants et cumule ses gains
            if (tirages[index].tirageTermine == true) {
                for (uint i=0; i < tirages[index].gagnants.length ; i++) {
                    if (tirages[index].gagnants[i] == msg.sender) {
                        // gain = total gain / tickets gagnants
                        uint256 gain = tirages[index].countNbTickets.mul(100 finney).div(tirages[index].countNbTicketsGagnants);
                        totalGain = totalGain.add(gain);
                       
                        tirages[index].gagnants[i] = address(0); // grille cette adresse
                    }
                }
            }
           
            d = d.add(5 days);
        }
       
        if (totalGain > 0) msg.sender.transfer(totalGain);
    }
   
    function terminerTirage(uint256 index) private {
        require(tirages[index].tirageTermine == false, "Ce tirage est déja terminé");
        tirages[index].tirageTermine = true;
       
         // tire un nombre entre 1 et 255
        tirages[index].nombreTire = uint8(uint256(keccak256(abi.encode(blockhash(block.number), block.timestamp))) % 254 +1);
       
        // récolte les adresse des gagnants
        for (uint i=0; i<tirages[index].participants.length ; i++) {
            address a = tirages[index].participants[i];
           
            // gagnant ?
            if (tirages[index].nombreTire == tirages[index].participantsMap[a] ) {
                // ajoute cette adresse à la liste des gagnants
                tirages[index].gagnants.push(a);
                tirages[index].countNbTicketsGagnants = tirages[index].countNbTicketsGagnants.add(1); // +1
            }
        }
    }
   
   
}
