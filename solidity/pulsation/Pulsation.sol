pragma solidity ^0.5.4;

contract Pulsation {
    
    uint256 public battement; 
    string private message;
    
    constructor(string memory m) public {
        battement = 0;
        message = m;
    }
    
    function ajouterBattement() public returns(string memory m)  {
        battement++;
        return message;
    }
}

contract Pendule is Pulsation("dong") {
    
    Pulsation internal tic; // adresse du contrat Pulsation
    Pulsation internal tac;
    
    string[] public balancier;
    
    constructor() public {
        tic = new Pulsation("tic");
        tac = new Pulsation("tac");
    }
    
    // effectue k mouvements de balancier
    function mouvementsBalancier(uint256 k) public {
        for (uint256 i=0; i<k ; i++) {
            if (i %2 == 0) balancier.push(tic.ajouterBattement());
            else balancier.push(tac.ajouterBattement());
        }
    }
}