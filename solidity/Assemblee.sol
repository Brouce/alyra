pragma solidity ^0.4.25;
contract Assemblee {

    string public nomAssemblee;

    struct Decision {
        string descriptionDecision;
        address candidatAdmin; //  facultatif, candidat pour devenir admin
        uint votesPour; // 0 default
        uint votesContre; // 0 default
        uint dateDebut;
        mapping (address => bool) aVote;
        uint ferme; // 0 = votes ouverts par defaut, 1 = votes fermé
    }
    Decision[] public decisions;


    // Membres
    struct Membre {
        address adresse;
        uint blames; // nombre de blames
    }
    Membre[] public membresArray;
    mapping (address => uint) public membresMap; // uint contient l'index de membresArray


    // Administrateurs
    struct Administrateur {
        uint index; // index de adminIndex
        address adresse;
        uint dateNomination;
    }
    mapping (address => Administrateur) public adminMap;
    address[] public adminArray;


    constructor(string nom) public {
        nomAssemblee = nom;
        
        adminMap[msg.sender].adresse = msg.sender;
        adminMap[msg.sender].dateNomination = now;
        adminMap[msg.sender].index = adminArray.push(msg.sender) -1;
    }
    
    function estAdmin(address a) public returns(bool) {
        if (adminArray[adminMap[a].index] == a) {
            // révoque cet admin si le délai d'une semaine est dépassé, et qu'il restera au moins un admin
            if (now - adminMap[a].dateNomination >= 7*24*60*60 && adminArray.length>1) {
              deleteAdmin(a);
              return false;
            }
            else return true;
        }
    }

    function ajouterAdmin(address a) public {
        require(estAdmin(msg.sender), "Vous n'etes pas un administrateur");
        
        // Vérifie que cette nouvelle adresse n'est pas un admin
        require(adminMap[a].adresse != a, "Cet administrateur existe déja");
           
        adminMap[a].adresse = a;
        adminMap[a].dateNomination = now;
        adminMap[a].index = adminArray.push(a) -1;
    }

    function deminssionner() public {
        require(estAdmin(msg.sender), "Vous n'etes pas un administrateur");
        
        // Vérifie qu'il restera au moins un admin
        require(adminArray.length>1, "Vous ne pouvez pas démissionner");
        deleteAdmin(msg.sender);
    }
    
    function deleteAdmin(address a) private {
        // delete dans adminArray: remplace le trou du delete par le dernier element du tableau
        if (adminMap[a].index < adminArray.length) {
            // déplace le dernier élement de adminArray sur celui qu'on veut supprimer
            // écrase l'@ à supprimer par la dernière du tableau
            adminArray[adminMap[a].index] = adminArray[adminArray.length-1]; 
            // met à jour l'index de l'Administrateur déplacé
            adminMap[adminArray[adminArray.length-1]].index = adminMap[a].index;
        }
        adminArray.length--; // supprimera le dernier élement du tableau
        //delete adminArray[adminArray.length-1]; implicite
        delete adminMap[a]; // à cet adresse, met tous les champs de l'Administrateur à 0
    }
    
    function adminArray_length() public view returns(uint) {
        return adminArray.length; // debug
    }

    function blamer(address a) public {
        require(estAdmin(msg.sender), "Vous n'etes pas un administrateur");
        require(estMembre(a), "Vous n'etes pas un membre");
        require(msg.sender != a, "Interdit de se blamer soi meme");
        
        if (membresArray[membresMap[a]].blames == 0) 
            membresArray[membresMap[a]].blames = 1; // 1 blame
        else {
            // membre expulsé au second blame
            delete membresArray[membresMap[a]]; 
            delete membresMap[a];
        }
    }
    
    function rejoindre() public {
        require(!estMembre(msg.sender), "Vous etes déja membre");
        // .push retourne la taille du tableau, cet index est gardé dans membresMap
        membresMap[msg.sender] = membresArray.push(Membre(msg.sender, 0)) -1;
    }

    function estMembre(address a) public view returns (bool) {
        if (membresArray.length == 0) return false;
        else return membresArray[membresMap[a]].adresse == a;
    }

    function proposerDecision(string description, address candidatAdmin) public {
        require(estMembre(msg.sender), "Vous n'etes pas un membre");
        require(!estAdmin(candidatAdmin), "Cette adresse est déja un administrateur");
        decisions.push(Decision(description, candidatAdmin, 0, 0, now, 0)); // votes ouverts
    }
    
    function voter(uint indiceDecision, uint vote) public {
        require(estMembre(msg.sender), "Vous n'etes pas un membre");
        require(decisions[indiceDecision].ferme == 0, "Vote cloturé (décision)");
        require(now - decisions[indiceDecision].dateDebut <= 120, "Vote cloturé (date)"); // délai de vote 604800 sec
        require(decisions[indiceDecision].aVote[msg.sender] == false, "Vous avez deja voté");
        
        if (vote==1) {
            decisions[indiceDecision].votesPour +=1;
            decisions[indiceDecision].aVote[msg.sender] = true;
        } else if (vote==0) {
            decisions[indiceDecision].votesContre +=1;
            decisions[indiceDecision].aVote[msg.sender] = true;
        }
    }

    function fermerDecision(uint indiceDecision) public {
        require(estAdmin(msg.sender), "Vous n'etes pas un administrateur");
        decisions[indiceDecision].ferme = 1;
        
        // Election d'un nouvel admininstrateur
        require(decisions[indiceDecision].candidatAdmin != 0);
        if (comptabiliser(indiceDecision)>0) {
            ajouterAdmin(decisions[indiceDecision].candidatAdmin);
        }
    }

    function comptabiliser(uint indiceDecision) public view returns(int) {
        return int(decisions[indiceDecision].votesPour) - int(decisions[indiceDecision].votesContre);
    }
    
}